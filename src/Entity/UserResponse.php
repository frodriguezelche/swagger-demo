<?php
declare(strict_types=1);

namespace App\Entity;

/**
 * @OA\Schema(schema="user_response")
 */
final class UserResponse {
   public $userId;
   public $username;

    public function __construct(
        $userId,
        $username
    )
    {
        $this->userId = $userId;
        $this->username = $username;
    }
}