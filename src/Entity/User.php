<?php

declare(strict_types=1);

namespace App\Entity;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
final class User {
    /**
     * The user id
     * @var string
     * @OA\Property()
     */
    public $userId;

    /**
     * The user name
     * @var string
     * @OA\Property()
     */
    public $username;

}