<?php

namespace App\Controller;

use App\Entity\UserResponse;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Annotation\Route;

class UserGetController
{
    /**
     *
     * @OA\Tag(name="Users")
     *
     * @Route("/api/doc/{userId}", methods={"GET"})
     *
     * @Security(name="Bearer")
     *
     * @OA\Parameter(
     *    name="userId",
     *    in="path",
     *    required=true,
     *    description="Identifier of user"
     * )
     *
     * @OA\Parameter(
     *    name="username",
     *    in="path",
     *    required=true,
     *    description="Username"
     * )
     *
     * @OA\Response(
     *    response=200,
     *    description="User details",
     *    @OA\JsonContent(
     *        type="array",
     *         @Model(type=UserResponse::class)
     *    )
     * )
     */
    public function __invoke($userId, $username): UserResponse
    {
        return new UserResponse(
            $userId,
            $username
        );
    }
}
